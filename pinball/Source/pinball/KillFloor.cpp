// Fill out your copyright notice in the Description page of Project Settings.


#include "KillFloor.h"

// Sets default values
AKillFloor::AKillFloor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AKillFloor::BeginPlay()
{
	Super::BeginPlay();
	OnActorHit.AddDynamic(this, &AKillFloor::OnHit);
	
}

// Called every frame
void AKillFloor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AKillFloor::OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	ABall* Check = Cast<ABall>(OtherActor);
	if (Check)
	{
		GetWorld()->DestroyActor(OtherActor);
		NewBall->ActiveBall = nullptr;
	}
}