// Fill out your copyright notice in the Description page of Project Settings.


#include "NewBallMachine.h"

// Sets default values
ANewBallMachine::ANewBallMachine()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANewBallMachine::BeginPlay()
{
	Super::BeginPlay();
	Controller = Cast<APinballPlayerController>(GetWorld()->GetFirstPlayerController());
	if(!Controller)
	{
		UE_LOG(LogTemp,Error,TEXT("NO PLAYER CONTROLLER"));
	}
	else
	{
		Controller->Reset.AddDynamic(this,&ANewBallMachine::Lancia);
		Controller->Charge.AddDynamic(this, &ANewBallMachine::Carica);
	}
	Pusher = Cast<UMeshComponent>(this->GetDefaultSubobjectByName(TEXT("Push")));
	if (!Pusher)
	{
		UE_LOG(LogTemp, Error, TEXT("NO Pusher"));
	}
	else
	{
		Start =Pusher->GetRelativeLocation().Z;
		MoveTime= (FMath::Abs(End - Start)) / MaxSpeed;
		//UE_LOG(LogTemp, Error, TEXT("%f"),End);
	}
}

// Called every frame
void ANewBallMachine::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (BFire)
	{
		Timer += DeltaTime;
		Pusher->SetRelativeLocation(FVector(0,0,FMath::Lerp(Start-50,End,Timer*(1/(MoveTime/ChargeRate)))));
		if (Timer >= MoveTime / ChargeRate)
		{
			Timer = 0;
			BFire = false;
			BReset = false;
			Stop.Broadcast();
		}
	}
	else if (!BReset)
	{
		Timer += DeltaTime;
		Pusher->SetRelativeLocation(FVector(0, 0, FMath::Lerp(End, Start, Timer*(1/MoveTime))));
		if (Timer >= MoveTime)
		{
			Timer = 0;
			BReset = true;
		}
	}
	else if (BCharge) 
	{
		Timer += DeltaTime;
		Pusher->SetRelativeLocation(FVector(0, 0, FMath::Lerp(Start, Start-50, Timer * (1 /MaxChargeTime))));
		if (Timer >= MaxChargeTime)
		{
			Timer = MaxChargeTime;
		}
	}
}

void ANewBallMachine::Lancia()
{
	if (BCharge)
	{
		ChargeRate = Timer / MaxChargeTime;
		BCharge = false;
		Timer = 0;
		BFire = true;
		//UE_LOG(LogTemp, Error, TEXT("sda"));
	}
}

void ANewBallMachine::Carica()
{
	if (BReset)
	{
		if (!ActiveBall)
		{
			Play.Broadcast();
			ActiveBall = GetWorld()->SpawnActor<ABall>(Ball, GetActorLocation(), FRotator(0, 0, 0));
			BCharge = true;
		}
	}
}
