// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PinballPlayerController.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/Actor.h"
#include "NewBallMachine.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FStop);

UCLASS()
class PINBALL_API ANewBallMachine : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ANewBallMachine();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	APinballPlayerController* Controller = nullptr;
	UMeshComponent* Pusher = nullptr;

	float Timer = 0;
	bool BFire = false;
	bool BCharge = false;
	bool BReset = true;
	float Start = 0;
	float MoveTime = 0;
	
	FStop Stop;
	FStop Play;

	ABall* ActiveBall = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = "ActorSpawning")
		TSubclassOf<ABall> Ball;
	UPROPERTY(EditAnywhere)
		float MaxSpeed = 0;
	UPROPERTY(EditAnywhere)
		float End = 0;
	UPROPERTY(EditAnywhere)
		float MaxChargeTime = 0;
	float ChargeRate = 0;

	UFUNCTION()
		void Lancia();
	UFUNCTION()
		void Carica();
};
