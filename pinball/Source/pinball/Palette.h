// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Palette.generated.h"

UCLASS()
class PINBALL_API APalette : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APalette();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere)
		float UpSpeed = 0;
	UPROPERTY(EditAnywhere)
		float ResetSpeed = 0;
	UPROPERTY(EditAnywhere)
		float LeftMax = -40;
	UPROPERTY(EditAnywhere)
		float RightMax = 0;
};
