// Fill out your copyright notice in the Description page of Project Settings.


#include "PinballPlayerController.h"

void APinballPlayerController::BeginPlay()
{
	Super::BeginPlay();
    Player = Cast<APalette>(GetPawn());
    LeftPaddle=Cast<UMeshComponent>(Player->GetDefaultSubobjectByName(TEXT("Left")));
    RightPaddle= Cast<UMeshComponent>(Player->GetDefaultSubobjectByName(TEXT("Right")));
    if (!(Player || LeftPaddle || RightPaddle))
    {
        UE_LOG(LogTemp, Error, TEXT("Failed SetUp"));
    }
    else
    {
        LStart = LeftPaddle->GetRelativeRotation().Yaw;
        RStart = RightPaddle->GetRelativeRotation().Yaw;
    }
}
void APinballPlayerController::SetupInputComponent()
{
   
    Super::SetupInputComponent();
    Input = FindComponentByClass<UInputComponent>();
    
    Input->BindAction("left", IE_Pressed, this, &APinballPlayerController::Left);
    Input->BindAction("left", IE_Released, this, &APinballPlayerController::StopLTimer);
    Input->BindAction("right", IE_Pressed, this, &APinballPlayerController::Right);
    Input->BindAction("right", IE_Released, this, &APinballPlayerController::StopRTimer);
    Input->BindAction("NewBall", IE_Pressed, this, &APinballPlayerController::Load);
    Input->BindAction("NewBall", IE_Released, this, &APinballPlayerController::NewBall);
}

void APinballPlayerController::Left()
{
    if (!BLStarTimer)
    {
        BLStarTimer = true;
        LPoint = LeftPaddle->GetRelativeRotation().Yaw;
    }
}

void APinballPlayerController::Right()
{
    if (!BRStarTimer)
    {
        BRStarTimer = true;
        RPoint = RightPaddle->GetRelativeRotation().Yaw;
    }
}

void APinballPlayerController::NewBall()
{
     Reset.Broadcast();
}

void APinballPlayerController::StopRTimer()
{
    RTimer = 0;
    BRStarTimer = false;
}

void APinballPlayerController::StopLTimer()
{
    LTimer = 0;
    BLStarTimer = false;
}

void APinballPlayerController::Load()
{
    Charge.Broadcast();
}


void APinballPlayerController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if (BLStarTimer)
    {
        float LTest = (FMath::Abs(Player->LeftMax - LPoint)) / Player->UpSpeed;
        LeftPaddle->SetRelativeRotation(FRotator(0, FMath::Lerp(LPoint, Player->LeftMax,LTimer*(1/LTest)),0));
        LTimer += DeltaTime;
        if (LTimer >= LTest)
        {
           LTimer = LTest;
          //  BLStarTimer = false;
        }
    }
    else if(LeftPaddle->GetRelativeRotation().Yaw<LStart)
    {
        LeftPaddle->SetRelativeRotation(FRotator(0, LeftPaddle->GetRelativeRotation().Yaw+(Player->ResetSpeed *DeltaTime),0));
    }


    if (BRStarTimer)
    {
        float RTest =  ((FMath::Abs(Player->RightMax - RPoint)) / Player->UpSpeed);
        RightPaddle->SetRelativeRotation(FRotator(0,FMath::Lerp(RPoint, Player->RightMax, RTimer*(1/RTest)),0));
        RTimer += DeltaTime;
        if (RTimer >= RTest)
        {
           RTimer = RTest;
           // BRStarTimer = false;
        }
    }
    else if(RightPaddle->GetRelativeRotation().Yaw > RStart)
    {
        RightPaddle->SetRelativeRotation(FRotator(0, RightPaddle->GetRelativeRotation().Yaw - (Player->ResetSpeed * DeltaTime), 0));
    }
}