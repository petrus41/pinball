// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Palette.h"
#include "Ball.h"
#include "Components/InputComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/PlayerController.h"
#include "PinballPlayerController.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNewBall);
UCLASS()
class PINBALL_API APinballPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;
	virtual void Tick(float DeltaTime) override;

	float LTimer = 0;
	float RTimer = 0;
	float LStart = 0;
	float RStart = 0;
	float LPoint = 0;
	float RPoint = 0;
	UPROPERTY(BlueprintReadOnly)
		float Points = 0;
	float ResetSpeed = 160;
	bool BLStarTimer = false;
	bool BRStarTimer = false;

	FNewBall Reset;
	FNewBall Charge;
	APalette* Player = nullptr;
	UInputComponent* Input = nullptr;
	UMeshComponent* LeftPaddle = nullptr;
	UMeshComponent* RightPaddle = nullptr;

	void StopLTimer();
	void StopRTimer();
	void Left();
	void Right();
	void NewBall();
	void Load();
};
