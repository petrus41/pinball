// Fill out your copyright notice in the Description page of Project Settings.


#include "Rimbalzi.h"

// Sets default values
ARimbalzi::ARimbalzi()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARimbalzi::BeginPlay()
{
	Super::BeginPlay();
	OnActorHit.AddDynamic(this, &ARimbalzi::OnHit);
	Controller = Cast<APinballPlayerController>(GetWorld()->GetFirstPlayerController());
}

// Called every frame
void ARimbalzi::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ARimbalzi::OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	if (Controller)
	{
		if (Cast<ABall>(OtherActor)) 
		{
			Controller->Points += Points;
			Hitted.Broadcast();
			UE_LOG(LogTemp, Error, TEXT("%f"), Controller->Points);
		}
	}
}