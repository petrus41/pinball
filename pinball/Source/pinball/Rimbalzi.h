// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PinballPlayerController.h"
#include "Rimbalzi.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHit);

UCLASS()
class PINBALL_API ARimbalzi : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARimbalzi();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintAssignable)
		FHit Hitted;
	UPROPERTY(EditAnywhere)
		float Points = 0;
	APinballPlayerController* Controller = nullptr;
private:
	UFUNCTION()
		void OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);
};
