// Fill out your copyright notice in the Description page of Project Settings.


#include "Stop.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AStop::AStop()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AStop::BeginPlay()
{
	Super::BeginPlay();
	NewBall->Stop.AddDynamic(this, &AStop::Up);
	NewBall->Play.AddDynamic(this, &AStop::Down);
	
}

// Called every frame
void AStop::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AStop::Up()
{
	FindComponentByClass<UStaticMeshComponent>()->SetRelativeLocation(FVector(0,0,0));
}

void AStop::Down()
{
	FindComponentByClass<UStaticMeshComponent>()->SetRelativeLocation(FVector(0,0,-160));
}