// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NewBallMachine.h"
#include "GameFramework/Actor.h"
#include "Stop.generated.h"

UCLASS()
class PINBALL_API AStop : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStop();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		ANewBallMachine* NewBall = nullptr;

	UFUNCTION()
		void Up();
	UFUNCTION()
		void Down();
};
