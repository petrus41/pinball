// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "pinballGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PINBALL_API ApinballGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
